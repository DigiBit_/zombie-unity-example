// ========================================================================================
// 2018 Copyright � DigiBit LLC - All rights reserved
//
using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;

namespace ArkadeSdk
{
     internal class ArkadeCallBacks:MonoBehaviour 
     {
          void Awake() 
          {
               DontDestroyOnLoad(this.gameObject);
          }

          void Update()
          {
               ArkadeBlaster.UpdatePidData();
               ArkadeBlaster.UpdateButtonStates();
          }

          void OnApplicationPause(bool paused)
          {
               #if UNITY_IOS
               IosNativeCommunication.OnApplicationPaused(paused);
               #endif
          }

          //Handles Device Disconnection Callback
          private void OnDeviceDisconnected (string data )
          {
               ArkadeBlaster.connected = false;
               if (ArkadeBlaster.logsEnabled)
                    Debug.Log(ArkadeBlaster.TAG + "OnDeviceDisconnected");
               if (ArkadeBlaster.enablePluginScenes)
                    SceneManager.LoadScene("PluginStartup", LoadSceneMode.Additive);
               ArkadeBlaster.CallBackHandler(CallBacks.DEVICE_DISCONNECTED);
          }

          // Handles Device Connection Callback
          private void OnDeviceConnected ( string data )
          {
               StartCoroutine(onDeviceConnectedWait());
          }

          // Handles Delay for Primary Connection Callback
          private IEnumerator onDeviceConnectedWait()
          {
               yield return new WaitForSecondsRealtime(0.5f);
               ArkadeBlaster.connected = true;
               if (ArkadeBlaster.logsEnabled)
                    Debug.Log(ArkadeBlaster.TAG + "OnDeviceConnected");
               ArkadeBlaster.CallBackHandler(CallBacks.DEVICE_CONNECTED);
          }


          //Reset Motion State Response Handler
          private void ResetMotionStateResponse ( string data )
          {
               byte[] response = UtilityFunctionsPrivate.StringToByte (data);
               ErrorCodes errorCode;

               if (response [0] == 0x46)
                    errorCode = ErrorCodes.SUCCESS;
               else if (response [0] == 0x86)
                    errorCode = (ErrorCodes) response [4];
               else
                    return;

               if (ArkadeBlaster.logsEnabled && errorCode == ErrorCodes.SUCCESS)
                    Debug.Log(ArkadeBlaster.TAG + "ResetMotionStateResponse:: " + UtilityFunctions.DecodeToString(errorCode));
               else if (ArkadeBlaster.logsEnabled)
                    Debug.LogError(ArkadeBlaster.TAG + "ResetMotionStateResponse :: " + UtilityFunctions.DecodeToString(errorCode));

               ArkadeBlaster.CallBackHandler(CallBacks.RESET_MOTION_STATE_RESPONSE, errorCode);
          }
     }
}

