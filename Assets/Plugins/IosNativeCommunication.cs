﻿// ========================================================================================
// 2018 Copyright © DigiBit LLC - All rights reserved
//

using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;

namespace ArkadeSdk
{
     internal static class IosNativeCommunication
     {
          #if UNITY_IOS
          [ DllImport ( "__Internal" ) ]
          extern static public void SetObjectName(string objectName);

          [ DllImport ( "__Internal" ) ]
          extern static public bool SendCommand(string command);

          [ DllImport ( "__Internal" ) ]
          extern static private IntPtr GetPidData();

          [ DllImport ( "__Internal" ) ]
          extern static public void EnableLogs(bool enable);

          [ DllImport ( "__Internal" ) ]
          extern static public void OnApplicationPaused(bool paused);

          public static string GetDataFromDevice()
          {
               string dataFromDevice = "";
               dataFromDevice = Marshal.PtrToStringAnsi(GetPidData());
               return dataFromDevice;
          }
          #endif
     }
}