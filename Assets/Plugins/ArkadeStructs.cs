﻿// ========================================================================================
// 2018 Copyright © DigiBit LLC - All rights reserved
//
using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;

namespace ArkadeSdk
{
     //Plugin Error Code
     public enum ErrorCodes
     {
          ERROR_NOT_PREPARED                      = 0x00,
          ERROR_UNKNOWN_COMMAND,                //= 0x01
          ERROR_NO_TABLE_DEFINED,               //= 0x02
          ERROR_BAD_CMD_STRUCT,                 //= 0x03
          ERROR_OUT_OF_RANGE,                   //= 0x04
          ERROR_IMU_NOT_STATIC,                 //= 0x05
          ERROR_MAX_BYTES_PID_TABLE_EXCEEDED,   //= 0x06
          ERROR_TOO_MANY_PIDS_PIDLOC_EXCEEDED,  //= 0x07
          ERROR_NO_OF_PID_TABLE_EXCEEDED,       //= 0x08
          ERROR_SEC_DB_NOT_CONNECTED,           //= 0x09
          ERROR_DEVICE_NOT_CONNECTED,           //= 0x0A
          SUCCESS,                              //= 0x0B
          ERROR_ALREADY_IN_PROGRESS,            //= 0x0C
          ERROR_PREDEFINED_PID_TABLE_ERROR,     //= 0x0D
          ERROR_NOT_AVAILABLE,
     };

     //Game State Codes
     public enum GameStates
     {
          VICTORY   = 0x01,
          DEFEAT, //= 0x02
          START,  //= 0x03
     }

     //Damage State Codes
     public enum DamageStates
     {
          NONE                = 0x01,
          HEALING,          //= 0x02
          POISON_ATTACK,    //= 0x03
          TAKING_DAMAGE,    //= 0x04
          EXPLOSIVE,        //= 0x05
          FLASH_BANGS,      //= 0x06
     }

     //CallBacks
     internal enum CallBacks
     {
          DEVICE_CONNECTED                        = 0x00,
          DEVICE_DISCONNECTED,                  //= 0x01
          RESET_MOTION_STATE_RESPONSE,          //= 0x02
     };

     public static class UtilityFunctions
     {
          /// <summary>
          /// Decodes to string.
          /// </summary>
          /// <returns>The to string.</returns>
          /// <param name="ErrorCode">Error code.</param>
          public static string DecodeToString(ErrorCodes ErrorCode)
          {
               switch (ErrorCode)
               {
                    case ErrorCodes.ERROR_NOT_PREPARED:
                         return "Not Prepared";
                    case ErrorCodes.ERROR_UNKNOWN_COMMAND:
                         return "Unknown Command";
                    case ErrorCodes.ERROR_NO_TABLE_DEFINED:
                         return "Pid Table Not Defined";
                    case ErrorCodes.ERROR_BAD_CMD_STRUCT:
                         return "Bad Command Structure";
                    case ErrorCodes.ERROR_OUT_OF_RANGE:
                         return "Out Of Range";
                    case ErrorCodes.ERROR_MAX_BYTES_PID_TABLE_EXCEEDED:
                         return "Max Bytes Of PID Table Exceeded";
                    case ErrorCodes.ERROR_TOO_MANY_PIDS_PIDLOC_EXCEEDED:
                         return "Number Of PIDs Per Table Exceeded";
                    case ErrorCodes.ERROR_NO_OF_PID_TABLE_EXCEEDED:
                         return "Number Of PID Table Exceeded";
                    case ErrorCodes.ERROR_SEC_DB_NOT_CONNECTED:
                         return "Secondary DigiBit Not Connected";
                    case ErrorCodes.ERROR_DEVICE_NOT_CONNECTED:
                         return "Device Not Connected Or Not Ready";
                    case ErrorCodes.SUCCESS:
                         return "Success";
                    case ErrorCodes.ERROR_ALREADY_IN_PROGRESS:
                         return "Command Already In Progress";
                    case ErrorCodes.ERROR_PREDEFINED_PID_TABLE_ERROR:
                         return "Error in configuring predefined PID table";
                    case ErrorCodes.ERROR_NOT_AVAILABLE:
                         return "Error Command not available";
                    default:
                         return "Null";
               }

          }
     }

     internal static class UtilityFunctionsPrivate
     {
          public static string ByteToString(byte[] Command)
          {
               return BitConverter.ToString(Command).Replace("-", string.Empty).ToUpper();
          }

          public static byte[] StringToByte(string Command)
          {
               List<byte> ByteList = new List<byte>();

               for (int x = 0; (x + 2) <= Command.Length; x += 2)
               {
                    ByteList.Add(Convert.ToByte(Command.Substring(x, 2), 16));
               }

               return ByteList.ToArray();
          }
     }

     public static partial class ArkadeBlaster
     {
          public struct Button
          {
               public bool key;
               public bool keyDown;
               public bool keyUp;
          }

          public struct _Inputs
          {
               public int timeStamp;

               public Button buttonX;
               public Button buttonY;
               public Button buttonA;
               public Button buttonB;
               public Button buttonC;
               public Button buttonD;
               public Button buttonE;
               public Button buttonF;
               public Button fire;
               public Button thumb1;
               public Button thumb2;
               public Button forward1;
               public Button forward2;
               public Button backward;


               public Vector3 linearVelocity;
               //public Vector3 linearAcceleration;
               public Vector3 eulerAngle;
               public Vector3 angularVelocity;

               public bool turnLeft;
               public bool turnRight;
               public bool moveUp;
               public bool moveDown;
               public bool moveLeft;
               public bool moveRight;
               public bool moveForward;
               public bool moveBackward;
               public bool tap;
               public bool leanLeft;
               public bool leanRight;
               public bool reload;
               public bool pickUpObject;
               public bool celebrate;
               public bool swapWeapon;
               public bool shield;
               public bool knifeAttack;
          }

     }
}

