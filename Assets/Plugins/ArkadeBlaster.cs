// ========================================================================================
// 2018 Copyright � DigiBit LLC - All rights reserved
//
//#define DEBUG_DATA

using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
#if DEBUG_DATA
using System.Net.Sockets;
#endif

namespace ArkadeSdk
{
     public static partial class ArkadeBlaster
     {
#if DEBUG_DATA
          internal static UdpClient client = new UdpClient(62698);
#endif

          //Tag for Debugging 
          internal const string TAG = "Arkade-Unity-Plugin ";
          //bool for Checking if Init called
          private static bool initCalled = false;
          //for swipe motion calculations
          private static _Inputs InputsLast;
          // for Yaw calc
          private static float yawRawLast = 0.0f;
          private static float rollRawLast = 0.0f;
          // for Yaw calc
          private static float sumYaw = 0.0f;
          private static float sumRoll = 0.0f;
          /// <summary>
          /// If true enables plugin logs.
          /// </summary>
          internal static bool logsEnabled = false;
          /// <summary>
          /// This enables loading of plugin scenes on startup and disconnection of DigiBits.
          /// </summary>
          internal static bool enablePluginScenes = true;
          /// <summary>                                            
          /// True if Primary is connected.
          /// </summary>
          public static bool connected = false;
          //PID data variables
          /// <summary>
          /// Data of Primary.
          /// </summary>
          public static _Inputs Inputs;
          // <summary>
          // Multiplier for euler angles.
          // </summary>
          public static Vector3 eulerMultiplier;

          /* Add this Script to Game Object Named "DigiBit"*/
#pragma warning disable //disable warnings if action is not used
          /// <summary>
          /// Occurs when device disconnected.
          /// </summary>
          public static event Action OnDeviceDisconnected;
          /// <summary>
          /// Occurs when device connected.
          /// </summary>
          public static event Action OnDeviceConnected;
          /// <summary>
          /// Occurs when reset motion state response is received.
          /// </summary>
          public static event Action<ErrorCodes> ResetMotionStateResponse;
#pragma warning restore


          /// <summary>
          /// Initializes the plugin with options.
          /// </summary>
          /// <returns>The init.</returns>
          /// <param name="enableLogs">If set to <c>true</c> enables logs.</param>
          /// <param name="enableScenes">If set to <c>true</c> enables plugin scenes.</param>
          public static void Init(bool enableLogs = false, bool enableScenes = true)
          {
               if (initCalled)
               {
                    Debug.LogWarning("Arkade.Init() called again!");
                    return;
               }

               eulerMultiplier.x = 1.0f;
               eulerMultiplier.y = 1.0f;
               eulerMultiplier.z = 1.0f;

               initCalled = true;
               GameObject arkade = new GameObject("ArkadeCallBacks");
               arkade.AddComponent<ArkadeSdk.ArkadeCallBacks>();
               logsEnabled = enableLogs;
               enablePluginScenes = enableScenes;
               SetObjectName("ArkadeCallBacks");
               EnableNativeLogs(logsEnabled);
               if (enablePluginScenes)
                    SceneManager.LoadScene("PluginStartup", LoadSceneMode.Additive);
          }

          /* These Functions Switch between Android or iOS based on the selected build Environment */
          private static void SetObjectName(string objectName)
          {
#if UNITY_IOS
               if (Application.platform == RuntimePlatform.IPhonePlayer)
                    IosNativeCommunication.SetObjectName(objectName);
#else
               if (Application.platform == RuntimePlatform.Android)
                    AndroidNativeCommunication.SetObjectName(objectName);
#endif
          }

          private static bool SendCommand(string command)
          {
#if UNITY_IOS
               if (Application.platform == RuntimePlatform.IPhonePlayer)
                    return IosNativeCommunication.SendCommand(command);
               else
                    return false;
#elif UNITY_ANDROID
               if (Application.platform == RuntimePlatform.Android)
                    return AndroidNativeCommunication.SendCommand(command);
               else
                    return false;
#else
                    return false;
#endif
          }

          private static string GetPidData()
          {
#if UNITY_IOS
               if (Application.platform == RuntimePlatform.IPhonePlayer)
                    return IosNativeCommunication.GetDataFromDevice();
               else
               return "";
#elif UNITY_ANDROID
               if (Application.platform == RuntimePlatform.Android)
                    return AndroidNativeCommunication.GetPidData();
               else
                    return "";
#else
                    return "";
#endif
          }

          /// <summary>
          /// Enables the native logs.
          /// </summary>
          /// <param name="Value">If set to <c>true</c> enables native logs.</param>
          private static void EnableNativeLogs(bool value)
          {
#if UNITY_IOS
               if (Application.platform == RuntimePlatform.IPhonePlayer)
                    IosNativeCommunication.EnableLogs(value);
#else
               if (Application.platform == RuntimePlatform.Android)
                    AndroidNativeCommunication.EnableLogs(value);
#endif
          }
          /* These Functions Switch between Android or iOS based on the selected build Environment */

          /*Reset Motion State Command*/
          /// <summary>
          /// Resets the state of the motion.
          /// </summary>
          /// <returns>ErrorCodes</returns>
          public static ErrorCodes ResetMotionState()
          {
               if (!initCalled)
               {
                    Debug.LogError("Arkade.Init() call is required.");
                    return ErrorCodes.ERROR_NOT_PREPARED;
               }

               if (!connected)
                    return ErrorCodes.ERROR_DEVICE_NOT_CONNECTED;

               //build command
               byte[] command = { 0x06, 0x00 };

               //send commdand
               if (SendCommand(UtilityFunctionsPrivate.ByteToString(command)))
                    return ErrorCodes.SUCCESS;

               return ErrorCodes.ERROR_DEVICE_NOT_CONNECTED;
          }
          /*Reset Motion State Command*/

          /*StartEndGame*/
          /// <summary>
          /// Triggers internal Game start or end functions.
          /// </summary>
          /// <returns>ErrorCodes</returns>
          /// <param name="gameState">Game state.</param>
          public static ErrorCodes StartEndGame(GameStates gameState)
          {
               if (!initCalled)
               {
                    Debug.LogError("Arkade.Init() call is required.");
                    return ErrorCodes.ERROR_NOT_PREPARED;
               }

               if (!connected)
                    return ErrorCodes.ERROR_DEVICE_NOT_CONNECTED;

               //build command
               byte[] command = { 0x12, 0x02, 0x01, (byte)gameState };

               //send commdand
               if (SendCommand(UtilityFunctionsPrivate.ByteToString(command)))
                    return ErrorCodes.SUCCESS;

               return ErrorCodes.ERROR_DEVICE_NOT_CONNECTED;
          }
          /*StartEndGame*/

          /*SetHealth*/
          /// <summary>
          /// Triggers internal health change functions.
          /// </summary>
          /// <returns>ErrorCodes</returns>
          /// <param name="health">Player health</param>
          /// <param name="damageState">Damage state.</param>
          public static ErrorCodes SetHealth(byte health, DamageStates damageState)
          {
               if (!initCalled)
               {
                    Debug.LogError("Arkade.Init() call is required.");
                    return ErrorCodes.ERROR_NOT_PREPARED;
               }

               if (!connected)
                    return ErrorCodes.ERROR_DEVICE_NOT_CONNECTED;

               //build command
               byte[] command = { 0x12, 0x03, 0x02, health, (byte)damageState };

               //send commdand
               if (SendCommand(UtilityFunctionsPrivate.ByteToString(command)))
                    return ErrorCodes.SUCCESS;

               return ErrorCodes.ERROR_DEVICE_NOT_CONNECTED;
          }
          /*SetHealth*/

          /*SetFilterStrength*/
          /// <summary>
          /// Sets the strength of filtering on aiming.
          /// </summary>
          /// <returns>ErrorCodes</returns>
          /// <param name="strength">Strength of filter 0 = no filter, 100 = strongest filter.</param>
          public static ErrorCodes SetFilterStrength(byte strength)
          {
               if (!initCalled)
               {
                    Debug.LogError("Arkade.Init() call is required.");
                    return ErrorCodes.ERROR_NOT_PREPARED;
               }

               if (!connected)
                    return ErrorCodes.ERROR_DEVICE_NOT_CONNECTED;

               //build command
               byte[] command = { 0x12, 0x02, 0x03, strength };

               //send commdand
               if (SendCommand(UtilityFunctionsPrivate.ByteToString(command)))
                    return ErrorCodes.SUCCESS;

               return ErrorCodes.ERROR_DEVICE_NOT_CONNECTED;
          }
          /*SetHealth*/

          /// <summary>
          /// Updates device data.
          /// </summary>
          internal static void UpdatePidData()
          {
               if (!initCalled)
               {
                    Debug.LogError("arkade.Init() call is required.");
                    return;
               }

               string data = GetPidData();

               if (data == "")
                    return;

               byte[] dataBytes = UtilityFunctionsPrivate.StringToByte(data);

               if (!connected)
               {
                    Inputs.turnLeft = false;
                    Inputs.turnRight = false;
                    Inputs.moveLeft = false;
                    Inputs.moveRight = false;
                    Inputs.moveUp = false;
                    Inputs.moveForward = false;
                    Inputs.moveBackward = false;
                    Inputs.tap = false;
                    Inputs.moveDown = false;

                    Inputs.eulerAngle.Set(0, 0, 0);
                    Inputs.linearVelocity.Set(0, 0, 0);
                    //Inputs.angularVelocity.Set(0, 0, 0);
                    //Inputs.linearAcceleration.Set(0, 0, 0);

                    Inputs.buttonA.key = false;
                    Inputs.buttonB.key = false;
                    Inputs.buttonC.key = false;
                    Inputs.buttonD.key = false;
                    Inputs.buttonE.key = false;
                    Inputs.buttonF.key = false;
                    Inputs.buttonX.key = false;
                    Inputs.buttonY.key = false;
                    Inputs.thumb1.key = false;
                    Inputs.thumb2.key = false;
                    Inputs.fire.key = false;
                    Inputs.forward1.key = false;
                    Inputs.forward2.key = false;
                    Inputs.backward.key = false;

                    return;
               }

               //Update GPIO
               byte[] temp = { dataBytes[2], dataBytes[3] };
               BitArray bits = new BitArray(16);
               bits = new BitArray(temp);

               Inputs.buttonA.key = bits[0];
               Inputs.buttonB.key = bits[1];
               Inputs.buttonC.key = bits[2];
               Inputs.buttonD.key = bits[3];
               Inputs.buttonE.key = bits[4];
               Inputs.buttonF.key = bits[5];
               Inputs.buttonX.key = bits[6];
               Inputs.buttonY.key = bits[7];
               Inputs.thumb1.key = bits[8];
               Inputs.thumb2.key = bits[9];
               Inputs.fire.key = bits[10];
               Inputs.backward.key = bits[11];
               Inputs.forward1.key = bits[12];
               Inputs.forward2.key = bits[13];

               // NOTE:  The Unity3D angles are DIFFERENT than the rest of the system.
               //        This is done to match Unity's 3D coordinate system.
               Inputs.eulerAngle.z = -1.0f * (((short)((dataBytes[5] << 8) | dataBytes[4])) / 128.0f);
               Inputs.eulerAngle.x = ((short)((dataBytes[7] << 8) | dataBytes[6])) / 128.0f;
               Inputs.eulerAngle.y = -1.0f * (((short)((dataBytes[9] << 8) | dataBytes[8])) / 128.0f);

               // Add multiplier. 
               Inputs.eulerAngle.x *= eulerMultiplier.x;

               // Pitch is fixed between -90 and 90
               if (Inputs.eulerAngle.x > 90) Inputs.eulerAngle.x = 90;
               else if (Inputs.eulerAngle.x < -90) Inputs.eulerAngle.x = -90;

               Inputs.eulerAngle.y = MultiplyAndLimitAngle(-360, 360, eulerMultiplier.y, Inputs.eulerAngle.y, ref yawRawLast, ref sumYaw);

               Inputs.eulerAngle.z *= eulerMultiplier.z;

               // Roll is fixed between -180 and 180
               if (Inputs.eulerAngle.z < -180) Inputs.eulerAngle.z = -180;
               else if (Inputs.eulerAngle.z > 180) Inputs.eulerAngle.z = 180;

               // Update Move Actions
               temp = new byte[] { dataBytes[10], dataBytes[11] };
               bits = new BitArray(temp);

               Inputs.moveForward = bits[0];
               Inputs.moveBackward = bits[1];
               Inputs.moveRight = bits[2];
               Inputs.moveLeft = bits[3];
               Inputs.turnRight = bits[4];
               Inputs.turnLeft = bits[5];
               Inputs.moveUp = bits[6];
               Inputs.moveDown = bits[7];
               Inputs.tap = bits[8];
               Inputs.leanLeft = bits[9];
               Inputs.leanRight = bits[10];
               Inputs.reload = bits[11];
               Inputs.pickUpObject = bits[12];
               Inputs.celebrate = bits[13];
               Inputs.swapWeapon = bits[14];
               Inputs.shield = bits[15];
               Inputs.knifeAttack = bits[0];

               // NOTE:  The Unity3D angles are DIFFERENT than the rest of the system.
               //        This is done to match Unity's 3D coordinate system.
               Inputs.linearVelocity.z = -1.0f * (((sbyte)dataBytes[12]) * 4.0f);
               Inputs.linearVelocity.x = ((sbyte)dataBytes[13]) * 4.0f;
               Inputs.linearVelocity.y = -1.0f * (((sbyte)dataBytes[14]) * 4.0f);

               Inputs.angularVelocity.z = -1.0f * (((short)(((dataBytes[16] << 8) & 0x0700) | dataBytes[15])));
               Inputs.angularVelocity.z = (dataBytes[16] & 0x08) == 0 ? Inputs.angularVelocity.z : -1.0f * Inputs.angularVelocity.z;

               Inputs.angularVelocity.x = ((short)(((dataBytes[17] << 4) & 0x07F0) | (dataBytes[16] >> 4)));
               Inputs.angularVelocity.x = (dataBytes[17] & 0x80) == 0 ? Inputs.angularVelocity.x : -1.0f * Inputs.angularVelocity.x;

               //Inputs.angularVelocity.y = -1.0f * (((short)(((dataBytes[19] << 8) & 0x0700) | dataBytes[18])));
               //Inputs.angularVelocity.y = (dataBytes[19] & 0x08) == 0 ? Inputs.angularVelocity.y : -1.0f * Inputs.angularVelocity.y;

               //Inputs.timeStamp = ((short)((dataBytes[19] >> 4)));

#if DEBUG_DATA
               unsafe
               {
                    ushort timestamp = (ushort)(Time.time * 1000.0f);
                    byte[] sendData = new byte[100];
                    Buffer.BlockCopy(BitConverter.GetBytes(timestamp), 0, sendData, 0, sizeof(ushort));
                    Buffer.BlockCopy(BitConverter.GetBytes(Inputs.eulerAngle.x), 0, sendData, 2, sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(Inputs.eulerAngle.y), 0, sendData, 6, sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(Inputs.eulerAngle.z), 0, sendData, 10, sizeof(float));
                    client.Send(sendData, 100, "192.168.1.102", 62698);
               }
#endif
          }

          internal static float MultiplyAndLimitAngle(float lowerLimit, float upperLimit, float multiplier, float raw, ref float rawLast, ref float sum)
          {
               float delta = 0.0f;
               if (rawLast < -90 && raw > 90)
                    delta = (raw - rawLast) - 360;
               else if (rawLast > 90 && raw < -90)
                    delta = 360 - (rawLast - raw);
               else
                    delta = raw - rawLast;

               float deltaScaled = delta * multiplier;
               float add = sum + deltaScaled;
               if (add > upperLimit)
                    sum = add - 360;
               else if (add < lowerLimit)
                    sum = 360 + add;
               else
                    sum = add;

               rawLast = raw;
               return sum;
          }

          internal static void CheckButton(ref Button CurrentState, ref Button LastState)
          {
               if (CurrentState.key == true && LastState.key == false)
               {
                    CurrentState.keyDown = true;
                    CurrentState.keyUp = false;
               }
               else if (CurrentState.key == false && LastState.key == true)
               {
                    CurrentState.keyDown = false;
                    CurrentState.keyUp = true;
               }
               else
               {
                    CurrentState.keyDown = false;
                    CurrentState.keyUp = false;
               }

               LastState.key = CurrentState.key;

          }

          internal static void UpdateButtonStates()
          {
               CheckButton(ref Inputs.buttonA, ref InputsLast.buttonA);
               CheckButton(ref Inputs.buttonB, ref InputsLast.buttonB);
               CheckButton(ref Inputs.buttonC, ref InputsLast.buttonC);
               CheckButton(ref Inputs.buttonD, ref InputsLast.buttonD);
               CheckButton(ref Inputs.buttonE, ref InputsLast.buttonE);
               CheckButton(ref Inputs.buttonF, ref InputsLast.buttonF);
               CheckButton(ref Inputs.buttonX, ref InputsLast.buttonX);
               CheckButton(ref Inputs.buttonY, ref InputsLast.buttonY);
               CheckButton(ref Inputs.thumb1, ref InputsLast.thumb1);
               CheckButton(ref Inputs.thumb2, ref InputsLast.thumb2);
               CheckButton(ref Inputs.fire, ref InputsLast.fire);
               CheckButton(ref Inputs.backward, ref InputsLast.backward);
               CheckButton(ref Inputs.forward1, ref InputsLast.forward1);
               CheckButton(ref Inputs.forward2, ref InputsLast.forward2);
          }

          internal static void CallBackHandler(CallBacks callback, ErrorCodes errorCode = ErrorCodes.SUCCESS, string data = "")
          {
               switch (callback)
               {
                    case CallBacks.DEVICE_DISCONNECTED:
                         if (OnDeviceDisconnected != null)
                              OnDeviceDisconnected();
                         break;

                    case CallBacks.DEVICE_CONNECTED:
                         if (OnDeviceConnected != null)
                              OnDeviceConnected();
                         break;

                    case CallBacks.RESET_MOTION_STATE_RESPONSE:
                         if (ResetMotionStateResponse != null)
                              ResetMotionStateResponse(errorCode);
                         break;
               }
          }
     }
}