﻿// ========================================================================================
// 2018 Copyright © DigiBit LLC - All rights reserved
//
using UnityEngine;
using System.Collections;
using System;

namespace ArkadeSdk
{
     internal class AndroidNativeCommunication  
     {
          /*This class handles communication between Unity and Android native*/

          private static AndroidJavaObject javaObject;
          private static AndroidJavaClass javaClass;

          // Initializes connection with android native class
          public static void SetObjectName(string gameObjectName)
          {
               javaClass = new AndroidJavaClass ("games.arkade.androidplugin.ArkadeBlaster"); 
               javaObject = javaClass.CallStatic <AndroidJavaObject> ("getInstance");
               javaObject.Call ("setObjectName", gameObjectName);
               javaObject.Call("initializeSearch");
          }

          public static bool SendCommand(string command)
          {
               return javaObject.Call<bool>("sendCommand",command);
          }

          public static string GetPidData()
          {
               return javaObject.Call<string>("getPidData");
          }

          public static void EnableLogs(bool enable)
          {
               javaObject.Call("enableLogs", enable);	
          }
     }
}
