﻿// ========================================================================================
// 2018 Copyright © DigiBit LLC - All rights reserved
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using ArkadeSdk;

public class PluginSceneManager : MonoBehaviour {

     //edit this script as necessary but keep the isActive bool there to check for multiple instance of scene
     //game should be paused when digibits disconnect as this script does not handle game pause

     static bool isActive = false; //to prevent multiple instances of scene to load
     Scene currentScene;
     bool endScene;
     public GameObject panel;
     public bool playWithoutBlaster;


     void Awake()
     {
          if (isActive) 
          {
               SceneManager.UnloadSceneAsync ("PluginStartUp");
               return;
          }

          isActive = true;
     }

     void Start () 
     {
          currentScene = SceneManager.GetActiveScene();
          ManageCameraPosition ();
          ManageEventSystem ();
          ManageSortingOrder ();
     }



     void Update ()
     {
          if (endScene)
               return;
          if (ArkadeBlaster.connected || playWithoutBlaster) 
               ContinueGame ();

     }


     void ManageCameraPosition()
     {
          float maxZposition = 0.0f;
          if (currentScene.name != "PluginStartUp") {
               foreach (GameObject gameObj in currentScene.GetRootGameObjects()) 
               {
                    foreach (Transform t in gameObj.GetComponentsInChildren<Transform> (true)) 
                    {
                         if (maxZposition < t.position.z)
                              maxZposition = t.position.z;
                    }
               }
          }

          Transform CameraTransform = gameObject.GetComponentInChildren<Camera> ().gameObject.transform;
          CameraTransform.position = new Vector3 (CameraTransform.position.x, CameraTransform.position.y, maxZposition+1.0f);

     }

     void ManageEventSystem()
     {
          if (GameObject.FindObjectOfType<EventSystem> () == null)
               gameObject.GetComponentInChildren<EventSystem>(true).gameObject.SetActive(true);
     }

     void ManageSortingOrder()
     {
          int sortOrder = 1000; //need better way to find this in scene
          string sortingLayerName = SortingLayer.layers [SortingLayer.layers.Length - 1].name;
          panel.GetComponent<Canvas> ().sortingLayerName = sortingLayerName;
          panel.GetComponent<Canvas> ().sortingOrder= sortOrder;
     }

     public void ContinueGame()
     {
          endScene = true;
          SceneManager.UnloadSceneAsync("PluginStartUp");
     }

}
