﻿// ========================================================================================
// 2018 Copyright © DigiBit LLC - All rights reserved
//
#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using System.IO;

public class PostProcessBuild
{
     [PostProcessBuild]
     public static void OnPostprocessBuild (BuildTarget buildTarget, string pathToBuiltProject)
     {
          #if UNITY_IOS
          string pbxprojPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";

          PBXProject proj = new PBXProject();

          proj.ReadFromString(File.ReadAllText(pbxprojPath));

          string target = proj.TargetGuidByName("Unity-iPhone");

          proj.AddFrameworkToProject (target, "CoreBluetooth.framework", false);

          File.WriteAllText(pbxprojPath, proj.WriteToString());

          string plistPath = pathToBuiltProject + "/Info.plist";
          PlistDocument plist = new PlistDocument();
          plist.ReadFromString(File.ReadAllText(plistPath));

          PlistElementDict rootDict = plist.root;

          rootDict.SetString("NSBluetoothPeripheralUsageDescription", "Ble used to connect to Arkade hardware.");

          File.WriteAllText(plistPath, plist.WriteToString());
          #endif
          }
}
#endif