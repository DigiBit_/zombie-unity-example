﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class JoystickRight : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler {

     private Image bgImage, joystickImage;

     public Vector3 InputDirection { get; set; }

     private void Start()
     {
          bgImage = GetComponent<Image>();
          joystickImage = transform.GetChild(0).GetComponent<Image>();
          InputDirection = Vector3.zero;
     }

     public void OnDrag(PointerEventData ped)
     {
          //Debug.Log("On Drag");
          Vector2 pos = Vector2.zero;
          if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImage.rectTransform,
                                                                      ped.position, ped.pressEventCamera, out pos))
          {
               pos.x = (pos.x / bgImage.rectTransform.sizeDelta.x);
               pos.y = (pos.y / bgImage.rectTransform.sizeDelta.y);

               float x = pos.x * 2;
               float y = pos.y * 2;

               InputDirection = new Vector3(x, 0, y);
               InputDirection = (InputDirection.magnitude > 1) ? InputDirection.normalized : InputDirection;

               joystickImage.rectTransform.anchoredPosition =
                              new Vector3(InputDirection.x * bgImage.rectTransform.sizeDelta.x / 3,
                                          InputDirection.z * bgImage.rectTransform.sizeDelta.y / 3);
          }
     }

     public void OnPointerDown(PointerEventData ped)
     {
          HeroController.isFire = true;
     }

     public void OnPointerUp(PointerEventData ped)
     {
          HeroController.isFire = false;
          HeroController.isCanFire = true;
          InputDirection = Vector3.zero;
          joystickImage.rectTransform.anchoredPosition = Vector3.zero;
     }

}
