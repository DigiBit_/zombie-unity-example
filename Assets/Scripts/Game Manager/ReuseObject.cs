﻿using UnityEngine;
using System.Collections.Generic;

public class ReuseObject : MonoBehaviour {

     public static List<GameObject> listGenade, listZombieGreen, listZombieMelty, listZombieRig;
     public static GameObject effectBlood;

     // Use this for initialization
     void Start()
     {
          listGenade = new List<GameObject>();
          listZombieGreen = new List<GameObject>();
          listZombieMelty = new List<GameObject>();
          listZombieRig = new List<GameObject>();
     }
}
