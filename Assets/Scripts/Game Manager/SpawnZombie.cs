﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class SpawnZombie : MonoBehaviour {

     [SerializeField]
     Transform[] spawnPoint;
     [SerializeField]
     GameObject[] zombiePrefabs;
     [SerializeField]
     int maxZombie, startZombie;
     [SerializeField]
     GameObject[] bonus;

     public static List<GameObject> listZombieLife;

     int numberZombie, indexPoint, wave;
     bool isSpawn;

     // Use this for initialization
     void Start () 
     {
          wave = 0;
          numberZombie = indexPoint = 0;
          listZombieLife = new List<GameObject>();
     }

     // Update is called once per frame
     void Update () 
     {
          if (!isSpawn && listZombieLife.Count == 0)
          {
               numberZombie = 0;
               if (wave > 0 && GameManager.isGiveBonus)
               {
                    int x = Random.Range(0, bonus.Length);
                    bonus[x].GetComponent<ItemController>().SetBonus();
                    //Direction.obj.gameObject.SetActive(true);
                    //Direction.targetTrue = bonus[x].transform;
                    GameManager.isGiveBonus = false;
               }
               StartCoroutine(Spawn());
          }
     }

     IEnumerator Spawn()
    {
          isSpawn = true;
          yield return new WaitForSeconds(4);
          if (SceneManager.GetActiveScene().name!="PlayUntimedWithArkade")
               TimerManager.instance.StartTimer();
          //Debug.Log("Spawn");
          while (numberZombie < startZombie)
          {
               int i = Random.Range(0, 3);
               switch (i)
               {
                    case 0:
                         Spawn_Zombie(ReuseObject.listZombieGreen, zombiePrefabs[i]);
                    break;

                    case 1:
                         Spawn_Zombie(ReuseObject.listZombieMelty, zombiePrefabs[i]);
                    break;

                    case 2:
                         Spawn_Zombie(ReuseObject.listZombieRig, zombiePrefabs[i]);
                    break;
               }
               indexPoint = (indexPoint + 1) % spawnPoint.Length;
               numberZombie++;
          }
          isSpawn = false;
          wave = 1;
          startZombie++;
          startZombie = Mathf.Clamp(startZombie, 0, maxZombie);
    }

     void Spawn_Zombie(List<GameObject> list, GameObject prefab)
     {
          if (list.Count != 0)
          {
               list[0].transform.position = spawnPoint[indexPoint].position;
               list[0].transform.rotation = spawnPoint[indexPoint].rotation;

               list[0].GetComponent<UnityEngine.AI.NavMeshAgent>().speed += 0.3f;
               list[0].GetComponent<ZombieController>().speedAnim += 0.5f;

               list[0].SetActive(true);
               listZombieLife.Add(list[0]);
               list.RemoveAt(0);
          }
          else
          {
               GameObject obj = (GameObject)Instantiate(prefab,
               spawnPoint[indexPoint].position, spawnPoint[indexPoint].rotation);
               listZombieLife.Add(obj);
          }
     }

}
