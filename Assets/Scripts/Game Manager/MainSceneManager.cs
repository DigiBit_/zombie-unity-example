﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using ArkadeSdk;
using UnityEngine.UI;

public class MainSceneManager : MonoBehaviour {

    public GameObject loadingImage;
    public Image LoadingBar;
    public Text TextEulerMultiplierHorzontal;
    public Text TextEulerMultiplierVertical;
    private float EulerMultiplierHorzontal = 1.0f;
    private float EulerMultiplierVertical = 1.0f;

     private void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

     public void OnEulerMultiplierUpdateHorzontal(string newValue)
     {
          EulerMultiplierHorzontal = float.Parse(TextEulerMultiplierHorzontal.text);
          TextEulerMultiplierHorzontal.text = EulerMultiplierHorzontal.ToString("0.00");
     }

     public void OnEulerMultiplierUpdateVertical(string newValue)
     {
          EulerMultiplierVertical = float.Parse(TextEulerMultiplierVertical.text);
          TextEulerMultiplierVertical.text = EulerMultiplierVertical.ToString("0.00");
     }

     public void PlayGameClicked()
     {
        if (ButtonManager.instance.iButton == 1)
            ButtonManager.instance.ButtonPlay();

        loadingImage.SetActive(true);
          SceneManager.LoadScene(1);
     }

     public void PlayTimedBlasterClicked()
     {
          if (ButtonManager.instance.iButton == 1)
               ButtonManager.instance.ButtonPlay();

          loadingImage.SetActive(true);
          ArkadeBlaster.Init(true, false);
          ArkadeBlaster.eulerMultiplier.x = EulerMultiplierVertical;
          ArkadeBlaster.eulerMultiplier.y = EulerMultiplierHorzontal;
          Debug.Log("EULER: ArkadeBlaster.eulerMultiplier.x = " + ArkadeBlaster.eulerMultiplier.x);
          Debug.Log("EULER: ArkadeBlaster.eulerMultiplier.y = " + ArkadeBlaster.eulerMultiplier.y);
          Debug.Log("EULER: ArkadeBlaster.eulerMultiplier.z = " + ArkadeBlaster.eulerMultiplier.z);
          StartCoroutine(StartLoader(4));
     }

     public void PlayTimedClicked()
     {
        if (ButtonManager.instance.iButton == 1)
            ButtonManager.instance.ButtonPlay();

        loadingImage.SetActive(true);
        StartCoroutine(StartLoader(5));
     }

    AsyncOperation oper;

    IEnumerator StartLoader(int iIndex)
    {
        oper = SceneManager.LoadSceneAsync(iIndex);
        oper.allowSceneActivation = false;

        while (!oper.isDone)
        {
            LoadingBar.fillAmount = oper.progress;
            yield return new WaitForEndOfFrame();
            if (oper.progress >= 0.9f) break;
        }

        yield return new WaitForEndOfFrame();
        //yield return new WaitForSeconds(3f);

        LoadingBar.fillAmount = 1f;
        oper.allowSceneActivation = true;
        yield return null;
    }
}
