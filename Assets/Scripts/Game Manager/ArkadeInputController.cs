﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ArkadeSdk;


public class ArkadeInputController : MonoBehaviour {

     bool TiltEnabled = false;
     bool reloadPressed = false;
     bool yawReset = false;

     void Update()
     {
          WeaponControlUpdate();
          AimControlUpdate();
          CharacterControlUpdate();
     }


     void WeaponControlUpdate()
     {
          if (GameManager.instance.isPause)
               return;

          //Gun Fire
          if (ArkadeBlaster.Inputs.fire.key)
               HeroController.instance.ClickFire();
          else
               HeroController.instance.EndFire();

          //Throw Grenade
          if (ArkadeBlaster.Inputs.buttonE.keyUp && !yawReset)
               HeroController.instance.ClickThrowGrenade();

          //Reload Gun
          if (ArkadeBlaster.Inputs.reload)
          {
               if (reloadPressed == false)
                    HeroController.instance.ClickReloadGun();
               reloadPressed = true;
          }
          else
               reloadPressed = false;

          if (ArkadeBlaster.Inputs.buttonF.keyDown && !reloadPressed)
               HeroController.instance.ClickReloadGun();

          ////Pause
          //if (ArkadeBlaster.Inputs.buttonX.keyDown)
               //GameManager.instance.ClickPause();

          //Peek Control
          if (ArkadeBlaster.Inputs.buttonY.keyDown)
               HeroController.instance.ImplicitGun(true);
          else if (!ArkadeBlaster.Inputs.buttonY.key)
               HeroController.instance.ImplicitGun(false);
     
          //Tilt Control
          if (!HeroController.instance.isTilting)
          {
               if (ArkadeBlaster.Inputs.leanRight && !TiltEnabled)
               {
                    HeroController.instance.EnableTilt(true);
                    TiltEnabled = true;
               }
               else if (ArkadeBlaster.Inputs.leanLeft && !TiltEnabled)
               {
                    HeroController.instance.EnableTilt(false);
                    TiltEnabled = true;
               }
               else if (TiltEnabled && !(ArkadeBlaster.Inputs.leanLeft || ArkadeBlaster.Inputs.leanRight))
               {
                    HeroController.instance.DisableTilt();
                    TiltEnabled = false;
               }
          }

          //Yaw Reset
          if (ArkadeBlaster.Inputs.buttonA.key && ArkadeBlaster.Inputs.buttonC.key &&
              ArkadeBlaster.Inputs.buttonE.key && !yawReset)
          {
               ArkadeBlaster.ResetMotionState();
               yawReset = true;
          }
          else if (!ArkadeBlaster.Inputs.buttonA.key && !ArkadeBlaster.Inputs.buttonC.key &&
                   !ArkadeBlaster.Inputs.buttonE.key)
          {
               yawReset = false;
          }
     }

     void AimControlUpdate()
     {
          if (GameManager.instance.isPause || !ArkadeBlaster.connected)
               return;

          HeroController.instance.Turn(ArkadeBlaster.Inputs.eulerAngle.x, ArkadeBlaster.Inputs.eulerAngle.y);
     }

     void CharacterControlUpdate()
     {
          if (GameManager.instance.isPause || !ArkadeBlaster.connected)
               return;

          float h = 0;
          float v = 0;
          float speed = 1.5f;

          if (ArkadeBlaster.Inputs.thumb2.key)
               v = -1;
          else if (ArkadeBlaster.Inputs.thumb1.key)
               v = 1;
          else
               v = 0;

          if (ArkadeBlaster.Inputs.forward1.key && ArkadeBlaster.Inputs.forward2.key)
          {
               h = -1.0f;
               speed = 5.0f;
          }
          else if (ArkadeBlaster.Inputs.forward1.key)
               h = -1.0f;
          else if (ArkadeBlaster.Inputs.backward.key)
          {
               h = 1;
               speed = 4.0f;
          }
          else
               h = 0;

          HeroController.instance.MoveCharacter(new Vector3(v, 0, -h), speed);
     }
}
