﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

     public Text textAmmo, textGrenade, textScore, textOverScore, textHealth, textYAW;

     // Use this for initialization
     void Start () 
     {
     }

     // Update is called once per frame
     void Update () 
     {
     }

     public void SetTextAmmo(string _text)
     {
          textAmmo.text = _text;
     }

     public void SetTextGrenade(int num)
     {
        textGrenade.text = num.ToString();
     }

     public void SetTextScore(int _score)
     {
          textScore.text = _score.ToString();
          textOverScore.text = _score.ToString();
     }

     public void SetImageGun(int _indexOld, int _indexNew)
     {
     }

     public void SetTextHealth(int heal)
     {
          textHealth.text = heal.ToString();
     }

}
