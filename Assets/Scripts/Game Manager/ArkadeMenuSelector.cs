﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArkadeSdk;

public class ArkadeMenuSelector : MonoBehaviour
{

     public Text PlayText;
     public Text QuitText;

    public Color normalColor;
    public Color selectedColor;

    int SelectedIndex = 0;

     void Update()
     {
          UpdateSelection();

          if (ArkadeBlaster.Inputs.fire.key || ArkadeBlaster.Inputs.buttonA.key || Input.GetKeyDown(KeyCode.Return))
               MakeSelection();

          if (Input.GetKeyDown(KeyCode.DownArrow)|| ArkadeBlaster.Inputs.buttonD.keyDown)
               SelectedIndex = 1;
          else if (Input.GetKeyDown(KeyCode.UpArrow)|| ArkadeBlaster.Inputs.buttonC.keyDown)
               SelectedIndex = 0;
     }

     void UpdateSelection()
     {
          switch (SelectedIndex)
          {
               case 0:
                PlayText.color = selectedColor;
                QuitText.color = normalColor;
               break;

               case 1:
                PlayText.color = normalColor;
                QuitText.color = selectedColor;
               break;

          }

     }

     void MakeSelection()
     {
          switch (SelectedIndex)
          {
            case 0:
                FindObjectOfType<StartSceneManager>().ClickPlay();
//                    GameManager.instance.ClickPlay();
                break;

            case 1:
                FindObjectOfType<StartSceneManager>().ClickQuit();
//                GameManager.instance.ClickQuit();
               break;

          }

     }
}
