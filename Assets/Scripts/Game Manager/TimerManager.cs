﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerManager : MonoBehaviour {

     public int timeOutValue; //TimeOut Value in Seconds
     public Text textTimer;
     int timerValue = 0;
     bool enableTimer = false;

     public static TimerManager instance;

     void Start () 
     {
          instance = this;
          StartCoroutine(Timer());
     }

     IEnumerator Timer()
     {
          while (true)
          {
               yield return new WaitForSeconds(1);
               if (enableTimer)
                    timerValue++;

               if ((timeOutValue - timerValue) <= 0)
               {
                    enableTimer = false;
                    TimeOutHandler();
               }
          }
     }

     void Update () 
     {
          //UI update
          textTimer.text = (timeOutValue - timerValue) + " s";
     }

     public void StartTimer()
     {
          timerValue = 0;
          enableTimer = true;
     }

     void TimeOutHandler()
     {
          HeroHealth.instance.TakeDame(100);
     }

}
