﻿using UnityEngine;
using UnityEngine.SceneManagement;
using ArkadeSdk;

public class GameManager : MonoBehaviour {

     [SerializeField]
     UIManager uiManager;
     [SerializeField]
     GameObject panelPlay, panelStart, panelGameOver, panelHelp, panelPause, loadingImage;
    
     public int numberGrenade;

     int score;
     public bool isHelp, isPause, isGameOver;

     public static bool isGiveBonus;

     public static GameManager instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () 
     {
           if (panelPlay != null && panelStart != null && panelGameOver != null && panelHelp != null && panelPause != null)
          {
               panelPlay.SetActive(true);
               panelStart.SetActive(false);
               panelGameOver.SetActive(false);
               panelHelp.SetActive(false);
               panelPause.SetActive(false);
          }
          isHelp = false;
          isPause = false;
        isGameOver = false;            
          isGiveBonus = true;
          score = -1;
          if (uiManager != null)
          {
               SetTextGrenade(numberGrenade);
               AddScore();
          }
     }

     // Update is called once per frame
     void Update () 
     {
     }

     public void AddScore()
     {
          score++;
          uiManager.SetTextScore(score);
          if (score== 28)
          {
               GameOver();
               ArkadeBlaster.StartEndGame(GameStates.VICTORY);
          }
     }

     public void SetAmmo(string ammo)
     {
          uiManager.SetTextAmmo(ammo);
     }

     public void SetImageGun(int _indexOld, int _indexNew)
     {
          uiManager.SetImageGun(_indexOld, _indexNew);
     }

     public void AddGrenade(int num)
     {
          numberGrenade += num;
          SetTextGrenade(numberGrenade);
     }

     public void DecreGrenade()
     {
          numberGrenade--;
          SetTextGrenade(numberGrenade);
     }

     public void SetTextGrenade(int num)
     {
          uiManager.SetTextGrenade(num);
     }

     public void SetTextHealth(int heal)
     {
          uiManager.SetTextHealth(heal);
     }

     public void GameOver()
     {
        Time.timeScale = 0;

          panelPlay.SetActive(false);
          panelGameOver.SetActive(true);
        isGameOver = true;
     }

     public void ClickReplay()
     {
          if (ButtonManager.instance.iButton == 1)
               ButtonManager.instance.ButtonPlay();

          isGameOver = false;
          Time.timeScale = 1;
          SceneManager.LoadScene(SceneManager.GetActiveScene().name);
//        SceneManager.LoadScene(1);
     }

     public void ClickHelp()
     {
        if (ButtonManager.instance.iButton == 1)
            ButtonManager.instance.ButtonPlay();

        isHelp = !isHelp;
          panelHelp.SetActive(isHelp);
          panelPlay.SetActive(!isHelp);
          Time.timeScale = (isHelp) ? 0 : 1;
     }

     public void ClickPause()
     {
        if (ButtonManager.instance.iButton == 1)
            ButtonManager.instance.ButtonPlay();

        isPause = !isPause;
          panelPause.SetActive(isPause);
          panelPlay.SetActive(!isPause);
          Time.timeScale = (isPause) ? 0 : 1;
     }

     //public void ClickPlay()
     //{
     //     loadingImage.SetActive(true);
     //   isGameOver = false;
     //     SceneManager.LoadScene(2);
     //}

     public void ClickMainMenu()
     {
        if (ButtonManager.instance.iButton == 1)
            ButtonManager.instance.ButtonPlay();

        isGameOver = false;
        SceneManager.LoadScene(1);
     }
}
