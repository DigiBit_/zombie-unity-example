﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using ArkadeSdk;
using UnityEngine.UI;

public class StartSceneManager : MonoBehaviour {

    public GameObject loadingImage;
    public Image LoadingBar;

    private void Start()
    {
        Time.timeScale = 1;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    public void ClickPlay()
    {
        if (ButtonManager.instance.iButton == 1)
            ButtonManager.instance.ButtonPlay();

        loadingImage.SetActive(true);
        SceneManager.LoadScene(2);
    }

    public void ClickQuit()
    {
        if (ButtonManager.instance.iButton == 1)
            ButtonManager.instance.ButtonPlay();

        Screen.sleepTimeout = SleepTimeout.SystemSetting;
        Application.Quit();
    }

    AsyncOperation oper;

    IEnumerator StartLoader(int iIndex)
    {
        oper = SceneManager.LoadSceneAsync(iIndex);
        oper.allowSceneActivation = false;

        while (!oper.isDone)
        {
            LoadingBar.fillAmount = oper.progress;
            yield return new WaitForEndOfFrame();
            if (oper.progress >= 0.9f) break;
        }

        yield return new WaitForEndOfFrame();
        //		yield return new WaitForSeconds(3f);

        LoadingBar.fillAmount = 1f;
        oper.allowSceneActivation = true;
        yield return null;
    }
}
