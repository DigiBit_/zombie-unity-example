﻿using UnityEngine;

public class ZombieController : MonoBehaviour {

     [SerializeField]
     GameObject ragdoll;
//     [SerializeField]
     public int zombieDam, idZombie;
     [SerializeField]
     AudioClip[] clips;

     Transform player;
     UnityEngine.AI.NavMeshAgent nav;
     Animator m_Animator;
     ZombieHealth m_EnemyHealth;
     AudioSource audioSource;
    int timerAttack;//, layerMask;

     [HideInInspector]
     public float speedAnim, speed;

     void OnEnable()
     {
          nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
          nav.enabled = true;
          speed = nav.speed;
     }

     // Use this for initialization
     void Start()
     {
          speedAnim = 1;
          audioSource = GetComponent<AudioSource>();
          timerAttack = 1;
          player = GameObject.FindGameObjectWithTag("Player").transform;
          m_Animator = GetComponent<Animator>();
          m_EnemyHealth = GetComponent<ZombieHealth>();
 //         layerMask = LayerMask.GetMask("enemy");
     }

     // Update is called once per frame
     void Update()
     {
          if (nav.enabled)
          {
               if (!m_EnemyHealth.isDie)
               {
                    nav.SetDestination(player.position);
                    if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("walk"))
                    {
                         m_Animator.speed = speedAnim;
                         timerAttack = 1;
                    }
               }
               else
               {
                    m_Animator.speed = 1;
                    PlayDie();
                    nav.enabled = false;
               }
          }
     }

     void OnTriggerStay(Collider col)
     {
          //Debug.Log("OnCollisionStay");
          if (col.gameObject.name == "Hero" && !m_EnemyHealth.isDie)
          {
               m_Animator.SetBool("Attack", true);
               m_Animator.speed = 1;
               nav.speed = 0;
               if (timerAttack == 0)
               {
                    //Debug.Log("Dam");
                    col.gameObject.GetComponent<HeroHealth>().TakeDame(zombieDam);
                    timerAttack = 1;
                    nav.speed = speed;
               }
          }
     }

     void OnTriggerExit(Collider col)
     {
          if (col.gameObject.name == "Hero")
          {
               nav.speed = speed;
               nav.enabled = true;
               m_Animator.SetBool("Attack", false);
               timerAttack = 1;
               m_Animator.speed = speedAnim;
          }
     }

     void PlayDie()
     {
          //m_Animator.SetBool("Attack", false);
          //m_Animator.SetBool("Die", true);
          EndDie();
     }

     public void EndDie()
     {
          switch (idZombie)
          {  
               case 0:
                    ReuseObject.listZombieGreen.Add(gameObject);
               break;

               case 1:
                    ReuseObject.listZombieMelty.Add(gameObject);
               break;

               case 2:
                    ReuseObject.listZombieRig.Add(gameObject);
               break;
          }
          SpawnZombie.listZombieLife.Remove(gameObject);
          Vector3 disPos = Vector3.zero;
          if (idZombie == 0)
          {
               disPos = new Vector3(transform.position.x, transform.position.y + 1.1f, transform.position.z);
          }
          else
          {
               disPos = new Vector3(transform.position.x, transform.position.y + 0.1f, transform.position.z);
          }
          gameObject.SetActive(false);
          Instantiate(ragdoll, disPos, transform.rotation);
     }

     public void DoDamage()
     {
          timerAttack = 0;
     }

     public void PlaySound(int index)
     {
          if (audioSource != null) 
          {
          audioSource.clip = clips [index];
          audioSource.Stop ();
          audioSource.Play ();
          }
     }

}
