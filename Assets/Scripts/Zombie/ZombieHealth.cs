﻿using UnityEngine;
using System.Collections;

public class ZombieHealth : MonoBehaviour {
    
     [SerializeField]
     float maxHealth;

     float currentHealth;
     GameManager gameManager;

     public bool isDie, isTakeDame;

     void OnEnable()
     {
          isDie = false;
          currentHealth = maxHealth;
          gameManager = FindObjectOfType<GameManager>();
     }

     // Use this for initialization
     void Start () 
     {
     }

     // Update is called once per frame
     void Update () 
     {
     }

     public void TakeDame(float _dam)
     {
          if (!isDie)
          {
               currentHealth -= Mathf.RoundToInt(_dam);
               currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
               if (currentHealth == 0)
               {
                    gameManager.AddScore();
                    isDie = true;
               }
          }
     }

     public void AddHealth(int _heal)
     {
         currentHealth += _heal;
         currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
     }

}
