﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour {

    public int iButton;

    [SerializeField] private AudioSource ButtonAudio1;

    /// Shared instance to access values
	public static ButtonManager instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start () {

        iButton = 1;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ButtonPlay()
    {
        if (!ButtonAudio1.isPlaying)
        {
            ButtonAudio1.Play();
        }
    }
}
