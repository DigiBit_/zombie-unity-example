﻿using UnityEngine;
using System.Collections;

public class ItemController : MonoBehaviour {

     [SerializeField]
     GameObject[] imageBonus;
     [SerializeField]
     GameObject parent;

     int idBonus;
     bool isCurrentBonus;

     // Use this for initialization
     void Start () 
     {
          isCurrentBonus = false;
     }

     // Update is called once per frame
     void Update () 
     {
     }

     public void SetBonus()
     {
          int x = Random.Range(0, imageBonus.Length);
          imageBonus[idBonus].SetActive(false);
          idBonus = x;
          imageBonus[idBonus].SetActive(true);
          parent.SetActive(true);
          isCurrentBonus = true;
     }

     void OnCollisionEnter(Collision col)
     {
          if (col.gameObject.name == "Hero" && isCurrentBonus)
          {
               HeroController hero = col.gameObject.GetComponent<HeroController>();
               switch (idBonus)
               {
                    case 0:
                         hero.AddHealth(40);
                    break;

                    case 1:
                         hero.AddGrenade(3);
                    break;

                    case 2:
                         int x = Random.Range(1, 3);
                         hero.AddAmmo(x);
                    break;
               }
               isCurrentBonus = false;
               parent.SetActive(false);
               GameManager.isGiveBonus = true;
//               Direction.obj.gameObject.SetActive(false);
          }
     }

}
