﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class PlayerTurn : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
     [SerializeField]
     Image objectCrosshair;
     [SerializeField]
     Camera cam;
     [SerializeField]
     float minRotateX, maxRotateX;

     Vector3 offset;
     float rotationY = 0.0f, rotationX = 0.0f, cameraSpeed = 20f;
     Quaternion ang;

     public static PlayerTurn instance;

     void Awake()
     {
          instance = this;
     }

     public void OnPointerDown(PointerEventData eventData)
     {
          Vector3 down = new Vector3(eventData.position.x, eventData.position.y, 0);
          offset = objectCrosshair.GetComponent<Image>().rectTransform.position - down;
     }

     public void OnDrag(PointerEventData eventData)
     {
          Vector3 drag = new Vector3(eventData.position.x, eventData.position.y, 0);
          Vector3 newPosition = offset + drag;

          rotationX = Mathf.Atan((objectCrosshair.GetComponent<Image>().rectTransform.position.y - newPosition.y) / 500) * Mathf.Rad2Deg;
          rotationY = cam.transform.localRotation.x + Mathf.Atan((newPosition.x - objectCrosshair.GetComponent<Image>().rectTransform.position.x) / 500) * Mathf.Rad2Deg;

          cam.transform.parent.Rotate(0, rotationY, 0);
          cam.transform.Rotate(rotationX, 0, 0);

          objectCrosshair.GetComponent<Image>().rectTransform.position = Vector3.Lerp(objectCrosshair.GetComponent<Image>().rectTransform.position, newPosition, cameraSpeed * Time.deltaTime);
     }

     public void OnPointerUp(PointerEventData eventData)
     {
          objectCrosshair.GetComponent<Image>().rectTransform.anchoredPosition = Vector3.zero;
     }
}
