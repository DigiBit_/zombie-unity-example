﻿using UnityEngine;
using System.Collections;

public class GrenadeController : MonoBehaviour {

     [SerializeField]
     GameObject boomEffect;
//     [SerializeField]
     public float dameBoom, time, radius;

     AudioSource audioSource;
     int layerMask;

     void OnEnable()
     {
          audioSource = GetComponent<AudioSource>();
          layerMask = LayerMask.GetMask("Shootable");
          //Debug.Log("Vao");
          StartCoroutine(DoDame());
     }

     IEnumerator DoDame()
     {
          yield return new WaitForSeconds(time);
          Instantiate(boomEffect, transform.position, transform.rotation);
          GetComponent<MeshRenderer>().enabled = false;
          AreaDamageEnemies(transform.position, radius, dameBoom);
          audioSource.Play();
          yield return new WaitForSeconds(audioSource.clip.length * 75 / 100);
          ReuseObject.listGenade.Add(gameObject);
          gameObject.SetActive(false);
     }

     void AreaDamageEnemies(Vector3 location, float radius, float damage)
     {
          Collider[] objectsInRange = Physics.OverlapSphere(location, radius, layerMask);
          //Debug.Log(objectsInRange.Length);
          foreach (Collider col in objectsInRange)
          {
               ZombieHealth enemy = col.GetComponent<ZombieHealth>();
               if (enemy != null && !enemy.isTakeDame)
               {
                    // linear falloff of effect
                    float proximity = Mathf.Abs(location.x - enemy.transform.position.x);
                    if (proximity != 0)
                         damage = dameBoom - 70 * (proximity / radius);
                    //Debug.Log(damage);
                    enemy.TakeDame(Mathf.RoundToInt(damage));
                    enemy.isTakeDame = true;
               }
          }
          foreach (Collider col in objectsInRange)
          {
               ZombieHealth enemy = col.GetComponent<ZombieHealth>();
               if (enemy != null && enemy.isTakeDame)
                    enemy.isTakeDame = false;
          }
    }
}
