﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HeroController : MonoBehaviour
{

     public Text textYaw;

     [SerializeField]
     GameManager gameManager;

     public GameObject[] guns, pointNomal, pointImplicit, lightGuns;
     [SerializeField]
     Camera cameraGun;

     public GameObject crossHair, holeFire, effectBlood, grenade, directionFirePoint;
     [SerializeField]
     JoystickLeft joyStickLeft;

     public float speedHero, forceThrow;

     public Camera cameraHero;

     RaycastHit shootHit;
     int indexGun, shootableMask;
     bool isImplicit, isThrowGrenade;//, isMove;
     float anpha, beta, angleMove, angleYfire, angleXfire;
     AudioSource audioSource;
     WeaponDetails detailGun;
     Animator animGun, animCrosshair;
     HeroHealth heroHealth;

     [SerializeField]
     GameObject cam;
     [SerializeField]
     float minRotateX, maxRotateX;

     public static HeroController instance;

     //for movement
     Rigidbody mRigid;

     public static bool isFire, isCanFire, isDoneDraw, isReload;

     bool isTilt = false;
     bool isLeft = false;
     bool isZooming = false;
     public bool isTilting = false;


     void Awake()
     {
          Application.targetFrameRate = 60;
          instance = this;
     }

     // Use this for initialization
     void Start()
     {
          angleXfire = angleYfire = 0;
          heroHealth = GetComponent<HeroHealth>();
          isCanFire = true;
          isDoneDraw = false;
          isFire = false;
          isImplicit = false;
          indexGun = 2;
          mRigid = GetComponent<Rigidbody>();
          animCrosshair = crossHair.GetComponent<Animator>();
          LoadGunDetail();
          shootableMask = LayerMask.GetMask("Shootable");
          audioSource = GetComponent<AudioSource>();
     }

     // Update is called once per frame
     void Update()
     {
          if (heroHealth.isDie)
          {
               animGun.SetBool("Fire", false);
               animGun.SetBool("Reload", false);
               return;
          }

#if UNITY_EDITOR
          float h = Input.GetAxis("Horizontal");
          float v = Input.GetAxis("Vertical");
          float speed = 1.5f;
          if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.UpArrow))
               speed = 5.0f;
          if (Input.GetKey(KeyCode.L))
               Turn(0, 180);
          else if (Input.GetKey(KeyCode.R))
               Turn(0, -180);
          else
               Turn(0, 0);

          MoveCharacter(new Vector3(h, 0, v), speed);
          if (Input.GetKey(KeyCode.Z))
               EnableTilt(true);
          else if (Input.GetKey(KeyCode.X))
               EnableTilt(false);
          else
               DisableTilt();

#else
               //MoveCharacter(new Vector3(0, 0, 1.0f));
               MoveCharacter(joyStickLeft.InputDirection);
#endif

          if (isDoneDraw && isFire && !isReload)
          {
               if (detailGun.currentAmmo > 0)
                    ClickFire();
               else if (detailGun.maxAmmoStart > 0)
                    ClickReloadGun();
               else
                    animGun.SetBool("Fire", false);
          }
          else
          {
               if (animGun.gameObject.activeSelf)
               {
                    animGun.SetBool("Fire", false);
               }
               if (angleXfire > 0 || angleYfire > 0)
               {
                    angleXfire -= Time.deltaTime * 5;
                    angleYfire -= Time.deltaTime * 5;
                    directionFirePoint.transform.localRotation = Quaternion.Euler(-angleYfire, angleXfire, 0);
               }
               else
               {
                    angleXfire = angleYfire = 0.0f;
                    directionFirePoint.transform.localRotation = Quaternion.Euler(-angleYfire, angleXfire, 0);
               }
          }

     }

     public void ClickNextGun()
     {
          if (!isThrowGrenade)
          {
               SaveGunDetail();
               if (isImplicit)
               {
                    ClickImplicitGun();
               }
               gameManager.SetImageGun(indexGun, (indexGun + 1) % 3);
               indexGun = (indexGun + 1) % 3;
               LoadGunDetail();
          }
     }

     public void ClickPreGun()
     {
          if (!isThrowGrenade)
          {
               SaveGunDetail();
               if (isImplicit)
               {
                    ClickImplicitGun();
               }
               gameManager.SetImageGun(indexGun, (indexGun + 2) % 3);
               indexGun = (indexGun + 2) % 3;
               LoadGunDetail();
          }
     }

     public void ClickImplicitGun()
     {
          if (!isReload && !isThrowGrenade && !isZooming && !isTilting)
          {
               crossHair.SetActive(isImplicit);
               isImplicit = !isImplicit;
               cameraGun.fieldOfView = (isImplicit) ? 10 : 60;

               if (isImplicit)
               {
                    guns[indexGun].transform.position = (isImplicit) ?
                    pointImplicit[indexGun].transform.position : pointNomal[indexGun].transform.position;
                    guns[indexGun].transform.rotation = (isImplicit) ?
                    pointImplicit[indexGun].transform.rotation : pointNomal[indexGun].transform.rotation;
                    StartCoroutine(AnimateZoom());
               }
               else
               {
                    cameraHero.fieldOfView = 60;

                    pointImplicit[indexGun].transform.localPosition = new Vector3(pointImplicit[indexGun].transform.localPosition.x,
                                                                                  pointImplicit[indexGun].transform.localPosition.y,
                                                                                  0.013f);
                    guns[indexGun].transform.position = (isImplicit) ? pointImplicit[indexGun].transform.position :
                                                                       pointNomal[indexGun].transform.position;
                    guns[indexGun].transform.rotation = (isImplicit) ? pointImplicit[indexGun].transform.rotation :
                                                                       pointNomal[indexGun].transform.rotation;
               }
          }
     }

     public void ImplicitGun(bool implicitGun)
     {
          if (!isReload && !isThrowGrenade && !isZooming && !isTilting)
          {
               crossHair.SetActive(!implicitGun);
               isImplicit = implicitGun;
               cameraGun.fieldOfView = (isImplicit) ? 10 : 60;

               if (isImplicit)
               {
                    guns[indexGun].transform.position = (isImplicit) ?
                    pointImplicit[indexGun].transform.position : pointNomal[indexGun].transform.position;
                    guns[indexGun].transform.rotation = (isImplicit) ?
                    pointImplicit[indexGun].transform.rotation : pointNomal[indexGun].transform.rotation;
                    StartCoroutine(AnimateZoom());
               }
               else
               {
                    cameraHero.fieldOfView = 60;

                    pointImplicit[indexGun].transform.localPosition = new Vector3(pointImplicit[indexGun].transform.localPosition.x,
                                                                                  pointImplicit[indexGun].transform.localPosition.y,
                                                                                  0.013f);
                    guns[indexGun].transform.position = (isImplicit) ? pointImplicit[indexGun].transform.position :
                                                                       pointNomal[indexGun].transform.position;
                    guns[indexGun].transform.rotation = (isImplicit) ? pointImplicit[indexGun].transform.rotation :
                                                                       pointNomal[indexGun].transform.rotation;
               }
          }
     }

     IEnumerator AnimateZoom()
     {
          isZooming = true;
          for (int i = 59; i >= 30; i--)
          {
               cameraHero.fieldOfView = i;
               guns[indexGun].transform.localPosition = new Vector3(guns[indexGun].transform.localPosition.x, guns[indexGun].transform.localPosition.y,
                                                                    guns[indexGun].transform.localPosition.z - 0.0075f);
               yield return new WaitForSeconds(0.01f);
          }

          isZooming = false;
     }

     public void TiltGun()
     {
          isTilt = !isTilt;

          if (isTilt)
          {
               if (isLeft)
               {
                    pointNomal[indexGun].transform.Rotate(new Vector3(-45, 0, 0));
                    pointImplicit[indexGun].transform.Rotate(new Vector3(-45, 0, 0));
                    pointImplicit[indexGun].transform.localPosition = new Vector3(-0.1f, 0.135f, guns[indexGun].transform.localPosition.z);
               }
               else
               {
                    pointNomal[indexGun].transform.Rotate(new Vector3(45, 0, 0));
                    pointImplicit[indexGun].transform.Rotate(new Vector3(45, 0, 0));
                    pointImplicit[indexGun].transform.localPosition = new Vector3(-0.135f, -0.1001f, guns[indexGun].transform.localPosition.z);

               }
          }
          else
          {
               if (isLeft)
               {
                    pointNomal[indexGun].transform.Rotate(new Vector3(45, 0, 0));
                    pointImplicit[indexGun].transform.Rotate(new Vector3(45, 0, 0));
                    pointImplicit[indexGun].transform.localPosition = new Vector3(-0.163f, 0.0231f, guns[indexGun].transform.localPosition.z);
               }
               else
               {
                    pointNomal[indexGun].transform.Rotate(new Vector3(-45, 0, 0));
                    pointImplicit[indexGun].transform.Rotate(new Vector3(-45, 0, 0));
                    pointImplicit[indexGun].transform.localPosition = new Vector3(-0.163f, 0.0231f, guns[indexGun].transform.localPosition.z);
               }
          }

          StartCoroutine(AnimateTilt());
     }

     IEnumerator AnimateTilt()
     {
          isTilting = true;
          for (int i = 0; i <= 10; i++)
          {
               if (isImplicit)
               {
                    guns[indexGun].transform.rotation = Quaternion.Lerp(guns[indexGun].transform.rotation, pointImplicit[indexGun].transform.rotation, 0.5f);
                    guns[indexGun].transform.position = Vector3.Lerp(guns[indexGun].transform.position, pointImplicit[indexGun].transform.position, 0.5f);
               }
               else
                    guns[indexGun].transform.rotation = Quaternion.Lerp(guns[indexGun].transform.rotation, pointNomal[indexGun].transform.rotation, 0.5f);
               yield return new WaitForSeconds(0.02f);
          }

          guns[indexGun].transform.rotation = (isImplicit) ? pointImplicit[indexGun].transform.rotation : pointNomal[indexGun].transform.rotation;
          guns[indexGun].transform.position = (isImplicit) ? pointImplicit[indexGun].transform.position : pointNomal[indexGun].transform.position;
          isTilting = false;
     }

     public void EnableTilt(bool _isLeft)
     {
          if (!isTilt && !isTilting && !isZooming)
          {
               isLeft = _isLeft;
               TiltGun();
          }
     }

     public void DisableTilt()
     {
          if (isTilt && !isTilting && !isZooming)
          {
               TiltGun();
          }
     }


     public void EnableImplicitGun()
     {
          if (!isImplicit)
          {
               ClickImplicitGun();
          }
     }

     public void DisableImplicitGun()
     {
          if (isImplicit)
          {
               ClickImplicitGun();
          }
     }

     public void ClickReloadGun()
     {
          if (detailGun.maxAmmoStart > 0 && detailGun.currentAmmo != detailGun.maxAmmoPerClip)
          {
               if (isImplicit)
               {
                    ClickImplicitGun();
               }
               animGun.SetBool("Reload", true);
               isReload = true;
          }
     }

     public void MoveCharacter(Vector3 directionMove, float speed = 4.0f)
     {
          if (directionMove != Vector3.zero)
          {
               //              isMove = true;
               anpha = Vector3.Angle(Vector3.forward, directionMove) * Mathf.Deg2Rad;
               beta = Vector3.Angle(Vector3.forward, transform.forward) * Mathf.Deg2Rad;
               //angle of new vector move
               angleMove = anpha * Mathf.Sign(directionMove.x) + beta * Mathf.Sign(transform.forward.x);
               directionMove = new Vector3(directionMove.magnitude * Mathf.Sin(angleMove), 0, directionMove.magnitude * Mathf.Cos(angleMove)).normalized;
               mRigid.MovePosition(mRigid.position + directionMove * speed * Time.deltaTime);
               animCrosshair.SetBool("Move", true);
          }
          else
          {
               //              isMove = false;
               if (!isFire && !gameManager.isPause && animCrosshair.gameObject.activeSelf)
               {
                    animCrosshair.SetBool("Move", false);

               }
          }
     }

     public void ClickFire()
     {
          isFire = true;
          if (isCanFire)
          {
               animGun.SetBool("Fire", true);
               isCanFire = false;
          }
     }

     public void EndFire()
     {
          isFire = false;
          isCanFire = true;

          animCrosshair.SetBool("Move", false);
     }

     public void DoFire()
     {
          //Debug.Log("Fire");

          animCrosshair.SetBool("Move", true);
          StartCoroutine(ShowLightGun());
          animGun.SetBool("Fire", false);

          Random.InitState((int)System.DateTime.Now.Ticks);

          angleXfire += Time.deltaTime * detailGun.hitchX;
          angleYfire += Time.deltaTime * detailGun.hitchX;

          angleXfire = Random.Range(-angleXfire, angleXfire);

          angleXfire = Mathf.Clamp(angleXfire, -detailGun.maxAngleFireX, detailGun.maxAngleFireX);
          angleYfire = Mathf.Clamp(angleYfire, 0, detailGun.maxAngleFireY);

          directionFirePoint.transform.localRotation = Quaternion.Euler(-angleYfire, angleXfire, 0);

          Ray shootRay = new Ray();

          shootRay.origin = directionFirePoint.transform.position;
          shootRay.direction = directionFirePoint.transform.forward;
          if (Physics.Raycast(shootRay, out shootHit, 500, shootableMask))
          {
               if (shootHit.collider.tag == "1")
               {
                    //Debug.Log(shootHit.collider.name + "1");
                    DoDame(1, shootHit.transform);
               }
               else if (shootHit.collider.tag == "2")
               {
                    //Debug.Log(shootHit.collider.name + "2");
                    DoDame(2, shootHit.transform);
               }
               else if (shootHit.collider.tag == "3")
               {
                    //Debug.Log(shootHit.collider.name + "3");
                    DoDame(3, shootHit.transform);
               }
               else if (shootHit.collider.tag == "4")
               {
                    //Debug.Log(shootHit.collider.name + "4");
                    DoDame(4, shootHit.transform);
               }
               else
               {
                    //Debug.Log(transform.forward);
                    GameObject obj = (GameObject)Instantiate(holeFire, shootHit.point, transform.rotation);
                    obj.transform.forward = -transform.forward;
               }
          }
          detailGun.currentAmmo--;
          gameManager.SetAmmo((indexGun == 0) ? (detailGun.currentAmmo + "/--") : (detailGun.currentAmmo + "/" + detailGun.maxAmmoStart));
     }

     IEnumerator ShowLightGun()
     {
          lightGuns[indexGun].SetActive(true);
          yield return new WaitForSeconds(0.05f);
          lightGuns[indexGun].SetActive(false);
     }

     void DoDame(int _index, Transform obj)
     {
          // 1: head, 2: spine, 3: hand, 4:leg
          ZombieHealth health = obj.GetComponent<ZombieHealth>();
          switch (_index)
          {
               case 1:
                    health.TakeDame(detailGun.dam * 4);
                    break;

               case 2:
                    health.TakeDame(detailGun.dam * 1.5f);
                    break;

               case 3:
                    health.TakeDame(detailGun.dam);
                    break;

               case 4:
                    health.TakeDame(detailGun.dam);
                    break;
          }

          if (ReuseObject.effectBlood != null)
          {
               ReuseObject.effectBlood.transform.position = shootHit.point;
               ReuseObject.effectBlood.transform.rotation = Quaternion.Euler(transform.rotation * Vector3.back);
          }
          else
          {
               ReuseObject.effectBlood = (GameObject)
               Instantiate(effectBlood, shootHit.point, Quaternion.Euler(transform.rotation * Vector3.back));
          }
     }

     public void SaveGunDetail()
     {
          guns[indexGun].SetActive(false);
          isReload = false;

          if (animGun.gameObject.activeSelf)
          {
               animGun.SetBool("Reload", false);
          }
          isDoneDraw = false;
     }

     public void LoadGunDetail()
     {
          guns[indexGun].SetActive(true);
          animGun = guns[indexGun].GetComponent<Animator>();
          detailGun = guns[indexGun].GetComponent<WeaponDetails>();
          gameManager.SetAmmo((indexGun == 0) ? (detailGun.currentAmmo + "/--") : (detailGun.currentAmmo + "/" + detailGun.maxAmmoStart));
     }

     public void PlaySound(AudioClip clip)
     {
          if (!heroHealth.isDie)
          {
               audioSource.clip = clip;
               audioSource.Stop();
               audioSource.Play();
          }
     }

     public void DoneReload()
     {
          isReload = false;
          animGun.SetBool("Reload", false);
          isCanFire = true;
          detailGun.maxAmmoStart += detailGun.currentAmmo;
          detailGun.currentAmmo = detailGun.maxAmmoStart;
          detailGun.currentAmmo = Mathf.Clamp(detailGun.currentAmmo, 0, detailGun.maxAmmoPerClip);
          detailGun.maxAmmoStart = (indexGun == 0) ? 100 : detailGun.maxAmmoStart - detailGun.currentAmmo;
          gameManager.SetAmmo((indexGun == 0) ? (detailGun.currentAmmo + "/--") : (detailGun.currentAmmo + "/" + detailGun.maxAmmoStart));
     }

     public void ClickThrowGrenade()
     {
          if (gameManager.numberGrenade > 0)
          {
               if (isImplicit)
               {
                    ClickImplicitGun();
               }
               guns[indexGun].SetActive(false);
               guns[3].SetActive(true);
               isReload = false;

               if (animGun.gameObject.activeSelf)
               {
                    animGun.SetBool("Reload", false);
               }
               isThrowGrenade = true;
          }
     }

     public void DoThrowGrenade()
     {
          if (ReuseObject.listGenade.Count != 0)
          {
               ReuseObject.listGenade[0].transform.position = transform.position;
               ReuseObject.listGenade[0].SetActive(true);
               ReuseObject.listGenade[0].GetComponent<Rigidbody>().AddForce(cameraGun.transform.forward * forceThrow, ForceMode.Impulse);
               ReuseObject.listGenade.RemoveAt(0);
          }
          else
          {
               GameObject gre = (GameObject)Instantiate(grenade, transform.position, transform.rotation);
               gre.GetComponent<Rigidbody>().AddForce(cameraGun.transform.forward * forceThrow, ForceMode.Impulse);
          }
          guns[indexGun].SetActive(true);
          guns[3].SetActive(false);
          isThrowGrenade = false;
          gameManager.DecreGrenade();
     }

     public void AddHealth(int num)
     {
          heroHealth.AddHealth(num);
     }

     public void AddGrenade(int num)
     {
          gameManager.AddGrenade(num);
     }

     public void AddAmmo(int index)
     {
          if (index == indexGun)
          {
               detailGun.maxAmmoStart += detailGun.maxAmmoPerClip * 2;
               gameManager.SetAmmo((indexGun == 0) ? (detailGun.currentAmmo + "/--") : (detailGun.currentAmmo + "/" + detailGun.maxAmmoStart));
          }
          else
          {
               guns[index].GetComponent<WeaponDetails>().maxAmmoStart += guns[index].GetComponent<WeaponDetails>().maxAmmoPerClip * 2;
          }
     }


     public void Turn(float X, float Y)
     {
          cam.transform.rotation = Quaternion.Euler(new Vector3(X, Y, 0));
     }
}
