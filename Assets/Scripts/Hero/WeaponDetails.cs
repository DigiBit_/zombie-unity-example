﻿using UnityEngine;
using System.Collections;

public class WeaponDetails : MonoBehaviour {

     [SerializeField]
     HeroController heroControll;

     public AudioClip[] clip; // 0: draw, 1: fire, 2: out, 3: in
     public int dam, maxAmmoPerClip, currentAmmo, totalAmmo, maxAmmoStart, hitchX, hitchY, numberBulletin1;
     public float maxAngleFireX, maxAngleFireY;

     // Use this for initialization
     void Start () 
     {
     }

     // Update is called once per frame
     void Update () 
     {
     }

     // call in animation
     public void DoneDraw()
     {
          HeroController.isDoneDraw = true;
          HeroController.isCanFire = true;
     }

     public void DoneReload()
     {
          heroControll.DoneReload();
     }

     public void DoFire()
     {
          heroControll.DoFire();
     }

     public void PlaySound(int index)
     {
          heroControll.PlaySound(clip[index]);
     }

     public void DoThrowGrenade()
     {
          heroControll.DoThrowGrenade();
     }

}
