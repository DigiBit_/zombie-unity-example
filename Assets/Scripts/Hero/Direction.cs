﻿using UnityEngine;
using System.Collections;

public class Direction : MonoBehaviour {

//     [SerializeField]
     public GameObject hero, pointTargetVirtual;

     public static Transform targetTrue, obj;

     float angle;

     // Use this for initialization
     void Start () 
     {
          obj = this.transform;
          gameObject.SetActive(false);
     }

     // Update is called once per frame
     void Update () 
     {
          pointTargetVirtual.transform.position = new Vector3(targetTrue.position.x,
          hero.transform.position.y, targetTrue.position.z);
          //Debug.Log(pointTargetVirtual.transform.localPosition);
          //Debug.Log(Vector2.Angle(Vector2.up, new Vector2(pointTargetVirtual.transform.localPosition.x, pointTargetVirtual.transform.localPosition.z)));

          angle = Vector2.Angle(Vector2.up, new Vector2(pointTargetVirtual.transform.localPosition.x, 
                                                        pointTargetVirtual.transform.localPosition.z));
          transform.rotation = Quaternion.Euler(new Vector3(0, 180, angle * Mathf.Sign(pointTargetVirtual.transform.localPosition.x)));
     }
}
